FROM nginx:alpine

RUN addgroup -S -g 82 www-data && \
    addgroup nginx www-data && \
    mkdir -p /var/www && \
    mkdir -p /etc/letsencrypt/live/ttanttakun.org && \
    rm /etc/nginx/conf.d/default.conf

COPY ttanttakun.org.conf /etc/nginx/conf.d/
COPY wordpress.conf /etc/nginx/
COPY h5bp /etc/nginx/h5bp/
COPY docker-entry-point.sh /usr/local/bin/

EXPOSE 80 443

CMD ["docker-entry-point.sh"]
