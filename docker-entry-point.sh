#!/bin/sh

# env var conditional example
if [ -z "$NGINX_SERVER_NAME" ]; then
    NGINX_SERVER_NAME=localhost
fi

# Set conf var
sed -i 's/PHP_HOST/'"${PHP_HOST}"'/' /etc/nginx/conf.d/*.conf

exec nginx -g "daemon off;"
